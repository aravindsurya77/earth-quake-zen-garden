import React from 'react';
import profileStyles from './styles/profile.module.css';

const Profile = ({profileDetails}) => {

    const renderProfileDetails = () => {
        return (
            <table className={profileStyles.tableContainer}>
                <tbody>
                    <tr>
                        <td className={profileStyles.profileHeader}>
                            First Name
                        </td>
                        <td className={profileStyles.profileDetail}>
                            {profileDetails.firstName}
                        </td>
                    </tr>
                    <tr>
                        <td className={profileStyles.profileHeader}>
                            Last Name
                        </td>
                        <td className={profileStyles.profileDetail}>
                            {profileDetails.lastName}
                        </td>
                    </tr>
                    <tr>
                        <td className={profileStyles.profileHeader}>
                            Phone
                        </td>
                        <td className={profileStyles.profileDetail}>
                            {profileDetails.phone}
                        </td>
                    </tr>
                    <tr>
                        <td className={profileStyles.profileHeader}>
                            Email
                        </td>
                        <td className={profileStyles.profileDetail}>
                            {profileDetails.email}
                        </td>
                    </tr>
                    <tr>
                        <td className={profileStyles.profileHeader}>
                            Bio
                        </td>
                        <td className={profileStyles.profileDetail}>
                            {profileDetails.bio}
                        </td>
                    </tr>
                </tbody>
            </table>
        );
    };

    return (
        <div className={profileStyles.profileContainer}>
            Profile
            <div className={profileStyles.profileDetails}>
                <img className={profileStyles.avatar} src={profileDetails.avatarImage} alt="profileAvatar"/>
                {renderProfileDetails()}
            </div>
        </div>
    );
};

export default Profile;