import React, {useState,} from 'react';
import './App.css';
import {data,} from './data.js';
import Header from './Header.jsx';
import EarthquakeLocations from './EarthquakeLocations.jsx';
import EarthquakeDetails from './EarthquakeDetails.jsx';
import Profile from './Profile.jsx';

const HOME_PAGE = 'homePage';
const EARTHQUAKE_DETAIL_PAGE = 'earthquakeDetailPage';
const PROFILE_PAGE = 'profilePage';

const App = () => {

    const [profile, setProfile] = useState({});
    const [pageKey, setPageKey] = useState('');
    const [earthquakeLocationDetails, setEarthquakeLocationDetails] = useState({});

    const getProfile = () => {
      setProfile(data.profile);
      setPageKey(PROFILE_PAGE);
    };

    const getEarthquakeLocationDetails = locationId => {
        const requiredLocation = data.data.features.find(feature => feature.id === locationId);
        const requiredLocationDetails = requiredLocation.properties;
        setEarthquakeLocationDetails(requiredLocationDetails);
        setPageKey(EARTHQUAKE_DETAIL_PAGE);
    };

    const renderContent = () => {
        return pageKey === PROFILE_PAGE ? <Profile profileDetails={profile}/>
        : pageKey === EARTHQUAKE_DETAIL_PAGE ? <EarthquakeDetails earthquakeLocationDetails={earthquakeLocationDetails}/>
        : <EarthquakeLocations tableData={data.data} goToDetailsPage={getEarthquakeLocationDetails}/>;
    };

  return (
      <div>
        <Header
            userName={data.profile.firstName}
            goToHome={() => setPageKey(HOME_PAGE)}
            openProfileClick={getProfile}
        />
        <div className="appContent">
            {renderContent()}
        </div>
      </div>
  );
};

export default App;
