import React from 'react';
import earthquakeDetailsStyles from './styles/earthquakeDetails.module.css';

const EarthquakeDetails = ({earthquakeLocationDetails}) => {

    const renderDetailsTable = () => {
        return (
            <table className={earthquakeDetailsStyles.tableContainer}>
                <tbody>
                    <tr>
                        <td className={earthquakeDetailsStyles.eqHeader}>
                            Place
                        </td>
                        <td className={earthquakeDetailsStyles.eqDetailPlace}>
                            {earthquakeLocationDetails.place}
                        </td>
                    </tr>
                    <tr>
                        <td className={earthquakeDetailsStyles.eqHeader}>
                            Magnitude
                        </td>
                        <td className={earthquakeDetailsStyles.eqDetail}>
                            {earthquakeLocationDetails.mag}
                        </td>
                    </tr>
                    <tr>
                        <td className={earthquakeDetailsStyles.eqHeader}>
                            Time
                        </td>
                        {/*the mock up seems to have the raw value for time, hence using the same
                        otherwise it is just a matter of converting it to required date format*/}
                        <td className={earthquakeDetailsStyles.eqDetail}>
                            {earthquakeLocationDetails.time}
                        </td>
                    </tr>
                        <tr>
                        <td className={earthquakeDetailsStyles.eqHeader}>
                            Status
                        </td>
                        <td className={earthquakeDetailsStyles.eqDetail}>
                            {earthquakeLocationDetails.status}
                        </td>
                    </tr>
                    <tr>
                        <td className={earthquakeDetailsStyles.eqHeader}>
                            Tsunami
                        </td>
                        <td className={earthquakeDetailsStyles.eqDetail}>
                            {earthquakeLocationDetails.tsunami}
                        </td>
                    </tr>
                    <tr>
                        <td className={earthquakeDetailsStyles.eqHeader}>
                            Type
                        </td>
                        <td className={earthquakeDetailsStyles.eqDetail}>
                            {earthquakeLocationDetails.type}
                        </td>
                    </tr>
                </tbody>
            </table>
        );
    };

    return (
        <div className={earthquakeDetailsStyles.earthquakeDetailsContainer}>
            {/*the mock up seems like it is rounding off the magnitude.,
            but I wasn't sure if it is really rounding off or just an overlook
            since there are no clear instructions, I am going with the actual value*/}
            M {earthquakeLocationDetails.mag} - {earthquakeLocationDetails.place}
            {renderDetailsTable()}
        </div>
    );
};

export default EarthquakeDetails;