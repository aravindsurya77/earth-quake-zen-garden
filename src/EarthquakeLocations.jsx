import React from 'react';
import earthquakeLocationsStyles from './styles/earthquakeLocations.module.css';

const EarthquakeLocations = ({tableData, goToDetailsPage}) => {

    const renderTableHeader = () => {
        return (
            <tr>
                <th className={earthquakeLocationsStyles.placeHeader}>
                    Place
                </th>
                <th className={earthquakeLocationsStyles.magHeader}>
                    Magnitude
                </th>
                <th className={earthquakeLocationsStyles.timeHeader}>
                    Time
                </th>
            </tr>
        );
    };

    const renderTableRows = () => {
        return tableData.features.map(feature => {
            return (
                <tr key={feature.id}>
                    <td onClick={() => goToDetailsPage(feature.id)} className={earthquakeLocationsStyles.linkToDetailsPage}>
                        {feature.properties.place}
                    </td>
                    <td>
                        {feature.properties.mag}
                    </td>
                    <td>
                        {(new Date(feature.properties.time)).toISOString()}
                    </td>
                </tr>
            );
        });
    };

    const renderEarthquakeLocationsTable = () => {
        return (
            <table className={earthquakeLocationsStyles.tableContainer}>
                <thead>
                    {renderTableHeader()}
                </thead>
                <tbody>
                    {renderTableRows()}
                </tbody>
            </table>
        );
    };

    return (
        <div className={earthquakeLocationsStyles.earthquakeLocationsContainer}>
            {tableData.metadata.title}
            <div>
                {renderEarthquakeLocationsTable()}
            </div>
        </div>
    );
};

export default EarthquakeLocations;