import React from 'react';
import {data,} from './data.js';
import headerStyles from './styles/header.module.css';

const Header = ({userName, goToHome, openProfileClick}) => {

    const headerLogo = data.site.logoImage;
    const title = data.site.title;

    return (
        <div className={headerStyles.headerElements}>
            <span
                className={headerStyles.logoContainer}
                onClick={goToHome}
            >
                <img className={headerStyles.logo} src={headerLogo} alt="logo"/>
            </span>
            <span className={headerStyles.title}>{title}</span>
            <span
                className={headerStyles.userName}
                onClick={openProfileClick}
            >Welcome {userName}</span>
        </div>
    );
};

export default Header;